import Testimonio from "./Testimonio";

function pruebaCall () {
    return (
        <div>
        <Testimonio
            nombremichi='Michi con Ponche y Aguinaldo'
            residencia='Mexico'
            carrera='Administracion'
            empresa='Casa'
            texto='Hola soy un michi licenciado, enfocado a administrar las croquetas que se dan
            de comer en mi casa, por lo tanto gestiono y controlo la cantidad de alimento para
            todos los michis de mi casa'
            imgmichi='michito.png' />
        </div>
    );
}

export default pruebaCall
